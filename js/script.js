window.options = {};
window.options.price = 0;
window.options.currency = 'ron.';
let url = window.cdn_path;
if (url === undefined) {
  url = '/';
}
const MSG_DELAY = 1,
  TYPE_SPEED = 1;
let flow = [
  {
    type: 'text',
    content:
      'Bună ziua, mă numesc Agnesa Ţuşcan. Sunt medic nutriționist, specialist în medicină funcțională și integrativă. Bun venit pe site-ul meu oficial.',
  },
  {
    type: 'text',
    content:
      'Aici ofer diagnostici online gratuite și recomandări personale care au ajutat să scadă în greutate la sute de femei și bărbați.',
  },
  {
    type: 'text',
    content:
      'Pentru a primi recomandările mele în privinţa scăderii în greutate, vă rugăm să răspundeți la câteva întrebări. ',
  },
  {
    type: 'single_choice',
    content: 'Cu câte kilograme doriţi să slăbiţi?',
    answers: ['Până la 5 kg', '5 - 10 kg', '10 - 15 kg', 'Mai mult de 15 kg'],
  },
  {
    type: 'single_choice',
    content: 'Ce părți ale corpului dvs. necesită corecție?',
    answers: [
      'Talia și burta',
      'Șolduri și fese',
      'Brațe și picioare',
      'Vreau să slăbesc peste tot',
    ],
  },
  {
    type: 'single_choice',
    content: 'Cât de activ trăiţi?',
    answers: [
      'Mă antrenez de 3-5 ori pe săptămână',
      'Mă antrenez de 1-3 ori pe săptămână',
      'Nu fac efort, dar încerc să merg pe jos',
      'Stil de viaţă sedentar',
    ],
  },
  {
    type: 'single_choice',
    content: 'Vă alimentaţi corect?',
    answers: [
      'Da, număr caloriile, nu mănânc dulciuri și făină',
      'Undeva 50/50',
      'Ador alimente „dăunătoare”, fast food și dulciurile',
      'Mănânc, pentru ce am destui bani',
    ],
  },
  {
    type: 'single_choice',
    content: 'Vârsta dvs.?',
    answers: [
      'până la 20 de ani',
      '20-30 de ani',
      '31-40 ani',
      '41-50 ani',
      'Peste 50 de ani',
    ],
  },
  {
    type: 'single_choice',
    content: 'Care este greutatea dvs. curentă?',
    answers: [
      'Până la 60 kg',
      '60 - 70 kg',
      '71 până la 80 kg',
      'Mai mult de 80 kg',
    ],
  },
  {
    type: 'single_choice',
    content: 'Înălțimea dvs.?',
    answers: [
      'Sub 150cm',
      '150-160 cm',
      '161-170 cm',
      '171-180 cm',
      'Peste 180 cm',
    ],
  },
  {
    type: 'text',
    content:
      'Mulțumesc. Aveți greutate excesivă, dar este posibil să vă atingeți scopul dorit de a pierde în greutate. ',
  },
  {
    type: 'text',
    content:
      'Acum, pentru a avea un corp de invidiat, nu este deloc necesar să vă chinuiți cu diete și antrenamente. Prima recomandare, care funcționează întotdeauna, este să beţi cel puțin 2 litri de apă pe zi. Apa ajută la normalizarea digestiei, la curățarea organismului de toxine',
  },
  {
    type: 'text',
    content:
      'Pentru a îmbunătăți efectul, beţi mai mult ceai verde și adăugaţi spirulină în dieta sa. Frunzele de ceai conțin catechine, care activează metabolismul organismului și elimină toxinele și sărurile de metale grele. Spirulina ajută la scăderea nivelului de lipide și colesterol.',
  },
  {
    type: 'text',
    content:
      'Pentru a începe procesul de ardere a grăsimilor, este necesară L-carnitina - oferă acizi grași mitocondriilor și ajută la procesarea lor în energie.',
  },
  {
    type: 'text',
    content:
      'Componentele enumerate pot fi utilizate separat, dar sunt mult mai convenabile și mai eficiente ca parte a suplimentului alimentar Keto Eat&amp;Fit. Atât spirulina, cât și ceaiul verde sunt prezente în el sub formă de extracte intense cu o concentrație maximă de nutrienți. ',
  },
  {
    type: 'text',
    content:
      'Pentru o scădere vizibilă a greutății corporale, este suficient să aruncați o capsulă într-un pahar cu apă cu 20 de minute înainte de masă și să o beți o dată dimineața, şi într-o lună veți putea să vă atingeți ţinta.',
  },
  {
    type: 'text',
    content: `Iată cum arată: <br><br> <img src='${url}images/tov.png' style='width:80%;max-width:180px'>`,
  },
  {
    type: 'text',
    content:
      'Odată cu descompunerea intensivă a grăsimii sub acțiunea capsulelor Keto Eat&amp;Fit., se eliberează energia utilă, care îi oferă corpului un impuls puternic, îndepărtează starea depresivă, ameliorează apatia și disperarea și îi crește performanța. Acest complex inovator nu are încă analogii!',
  },
  {
    type: 'text',
    content:
      'Keto Eat&amp;Fit. se descompun foarte eficient și îndepărtează grăsimile viscerale, care învăluie pereții organelor interne și le împiedică să funcționeze normal, deblochează metabolismul prin curățarea organismului de toxine.',
  },
  {
    type: 'text',
    content:
      'Ca urmare, organismul începe să scape de excesul de grăsime în sine, folosindu-l ca sursă de energie. Mâncați mai puțin fără să simțiți foame, ceea ce îmbunătățește și mai mult efectul de slăbire!',
  },

  {
    type: 'text',
    content:
      'Durata optimă a cursului, care ține cont de vârsta, indicele de masă corporală actuală și stilul de viață, este de la 30 de zile. ',
  },
  {
    type: 'text',
    content:
      'În acest timp, va avea loc o curățare profundă a organismului și normalizarea metabolismului.',
  },
  {
    type: 'text',
    content:
      'Mai am pentru voi vești grozave. Ați trecut diagnosticul online și ați devenit al două miilea meu client!',
  },
  {
    type: 'text',
    content:
      'Doar astăzi aveți ocazia să obțineți Keto Eat&amp;Fit. cu o reducere de 50% ca parte a promoției producătorului.',
  },
  {
    type: 'text',
    content:
      'Pentru a obține Keto Eat&amp;Fit., scrieți-vă numele și numărul de telefon în formularul de comandă de mai jos. Datele dvs. sunt trimise direct producătorului, nimeni altcineva nu are acces la el.',
  },
  {
    type: 'text',
    content:
      'Specialistul vă va suna înapoi și după clarificarea tuturor detaliilor, în aceeași zi vă va fi trimis pachetul cu cursul Keto Eat&amp;Fit.',
  },
  {
    type: 'text',
    content:
      'Numărul de pachete promoționale este limitat, așa că vă recomand să vă grăbiți cu comanda.',
  },
  { type: 'form', templateId: 'orderForm' },
];

function createSingleChoiceForm(t) {
  let e = t.reduce(
    (t, e) =>
      t + `<button type="button" class="answer" data-answer>${e}</button>`,
    ''
  );
  return $(`<div class="answers">${e}</div>`);
}

flow.reduce(
  (t, e) =>
    t.then(
      () =>
        new Promise((t) => {
          (t.Scroll = function () {
            $(document).ready(function () {
              !(function (t, e = function () {}, n = []) {
                (t = jQuery(t)), (e = e.bind(t));
                var o = -1,
                  c = -1;
                setInterval(function () {
                  (t.height() == o && t.width() == c) ||
                    ((o = t.height()),
                    (c = t.width()),
                    t.parent().animate({ scrollTop: o }, 50),
                    e.apply(null, n));
                }, 100);
              })(
                '.chat-content-container .chat-content-list',
                function (t) {},
                []
              );
            });
          }),
            setTimeout(() => {
              let n = 'rand_' + new Date().getTime(),
                o = $(`<div id='${n}' class="box"></div>`);
              switch (($('.container2').append(o), e.type)) {
                case 'single_choice':
                  new Typed(`#${n}`, {
                    strings: [e.content],
                    showCursor: !1,
                    typeSpeed: 50,
                    onComplete: () => {
                      let n = createSingleChoiceForm(e.answers);
                      n.find('[data-answer]').click((e) => {
                        $(e.target).addClass('active'),
                          n
                            .find('[data-answer]:not(.active)')
                            .attr('disabled', !0),
                          t();
                      }),
                        o.append(n),
                        e.afterMount && e.afterMount();
                    },
                  });
                  break;
                case 'form':
                  let c = $(`template#${e.templateId}`).html();
                  o.append(c), e.afterMount && e.afterMount(), t();
                  break;
                case 'text':
                  new Typed(`#${n}`, {
                    strings: [e.content],
                    showCursor: !1,
                    typeSpeed: 50,
                    onComplete: () => {
                      e.afterMount && e.afterMount(), t(), t.Scroll();
                    },
                  });
              }
            }, 50);
        })
    ),
  Promise.resolve()
);
